FROM arm32v6/alpine

COPY ./entrypoint.sh /
COPY qemu-arm-static /usr/bin

RUN apk add --no-cache dumb-init bash gitlab-runner && chmod +x /entrypoint.sh && mkdir /etc/gitlab-runner && touch /etc/gitlab-runner/config.toml

ENTRYPOINT ["/usr/bin/dumb-init", "/entrypoint.sh"]
CMD ["run", "--user=gitlab-runner"]